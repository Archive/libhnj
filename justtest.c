/* LibHnj is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* LibHnj - a library for high quality hyphenation and justification
 * Copyright (C) 1998 Raph Levien
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "hyphen.h"
#include "hsjust.h"

#include "hnjalloc.h"
/* Warning: this program doesn't do bounds checking right. */

char *
strdup_from_buf (const char *buf, int size)
{
  char *new;

  new = hnj_malloc (size + 1);
  memcpy (new, buf, size);
  new[size] = '\0';
  return new;
}

#define noPRINT_HYPHENS

void
hnj (char **words, int n_words, HyphenDict *dict, HnjParams *params)
{
  char hbuf[256];
  HnjBreak breaks[16384];
  int result[16384];
  int is[16384], js[16384];
  int n_breaks;
  int i, j;
  int x;
  int l;
  int n_actual_breaks;
  int line_num;
  int break_num;

  n_breaks = 0;
  x = 0;
  for (i = 0; i < n_words; i++)
    {
      l = strlen (words[i]);
      hnj_hyphen_hyphenate (dict, words[i], l, hbuf);
      for (j = 0; j < l; j++)
	{
#ifdef PRINT_HYPHENS
	  putchar (words[i][j]);
#endif
	  if (hbuf[j] & 1)
	    {
#ifdef PRINT_HYPHENS
	      putchar ('-');
#endif
	      breaks[n_breaks].x0 = x + (j + 1) * 10;
	      breaks[n_breaks].x1 = x + j * 10;
	      breaks[n_breaks].penalty = 2000;
	      breaks[n_breaks].flags = HNJ_JUST_FLAG_ISHYPHEN;
	      is[n_breaks] = i;
	      js[n_breaks] = j + 1;
	      n_breaks++;
	    }
	}
#ifdef PRINT_HYPHENS
      putchar (' ');
#endif
      breaks[n_breaks].x0 = x + l * 10;
      x += (l + 1) * 10;
      breaks[n_breaks].x1 = x;
      breaks[n_breaks].penalty = 0;
      breaks[n_breaks].flags = HNJ_JUST_FLAG_ISSPACE;
      is[n_breaks] = i;
      js[n_breaks] = l;
      n_breaks++;
    }
  breaks[n_breaks - 1].flags = 0;
  n_actual_breaks = hnj_hs_just (breaks, n_breaks,
				 params, result);

  i = 0;
  /* Now print the paragraph with the breaks present. */
  for (line_num = 0; line_num < n_actual_breaks; line_num++)
    {
      break_num = result[line_num];
      for (; i < is[break_num]; i++)
	{
	  printf ("%s ", words[i]);
	}
      if (breaks[break_num].flags & HNJ_JUST_FLAG_ISSPACE)
	{
	  printf ("%s\n", words[i++]);
	}
      else if (breaks[break_num].flags & HNJ_JUST_FLAG_ISHYPHEN)
	{
	  for (j = 0; j < js[break_num] && words[i][j]; j++)
	    putchar (words[i][j]);
	  printf ("-\n");
	  for (; words[i][j]; j++)
	    putchar (words[i][j]);
	  putchar (' ');
	  i++;
	}
      else
	printf ("%s\n\n", words[i++]);
   }
}

int
main (int argc, char **argv)
{
  HyphenDict *dict;
  char buf[256];
  HnjParams params;
  char *words[2048];
  int i;
  int beg_word;
  int word_idx;

  params.set_width = 720;
  params.max_neg_space = 5;
  dict = hnj_hyphen_load ("hyphen.mashed");

  word_idx = 0;
  /* Parse a paragraph into the words data structures. */
  while (fgets (buf, sizeof(buf), stdin))
    {
      if (buf[0] == '\n' && word_idx > 0)
	{
	  hnj (words, word_idx, dict, &params);
	  word_idx = 0;
	}
      beg_word = 0;
      for (i = 0; i < sizeof(buf) && buf[i] != '\n'; i++)
	{
	  if (isspace (buf[i]))
	    {
	      if (i != beg_word)
		words[word_idx++] = strdup_from_buf (buf + beg_word,
						     i - beg_word);
	      beg_word = i + 1;
	    }
	}
      if (i < sizeof(buf) && i != beg_word)
	words[word_idx++] = strdup_from_buf (buf + beg_word,
					     i - beg_word);
    }
  if (word_idx > 0)
    hnj (words, word_idx, dict, &params);
  return 0;
}
