/* LibHnj is dual licensed under LGPL and MPL. Boilerplate for both
 * licenses follows.
 */

/* LibHnj - a library for high quality hyphenation and justification
 * Copyright (C) 1998 Raph Levien
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the 
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, 
 * Boston, MA  02111-1307  USA.
*/

/*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.0 (the "MPL"); you may not use this file except in
 * compliance with the MPL.  You may obtain a copy of the MPL at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the MPL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the MPL
 * for the specific language governing rights and limitations under the
 * MPL.
 *
 */
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "hyphen.h"

int main (int argc, char **argv)
{
  HyphenDict *dict;
  char buf[80], hbuf[80];
  int i, j;

  dict = hnj_hyphen_load ("hyphen.mashed");
  if (dict != NULL)
    {
      while (fgets (buf, sizeof(buf), stdin))
	{
	  hnj_hyphen_hyphenate (dict, buf, strlen (buf), hbuf);
	  j = 0;
	  for (i = 0; buf[i]; i++)
	    {
	      putchar (buf[i]);
	      if (isalpha (buf[i]))
		{
		  if (hbuf[i] & 1)
		    putchar ('-');
		}
	    }
	}
    }
  else
    {
      printf ("can't load hyphenation dictionary!\n");
    }
  return 0;
}
